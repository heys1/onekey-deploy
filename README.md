#oneKey-Deploy

本项目是由java编写的基于ssh+sftp的一键部署小工具，用于开发/测试时快速将本地项目文件快速部署到服务器。
- 一键上传项目文件夹、文件到指定服务器的目录并重启web服务器
- 不上传相同文件，减少部署时间
- 提供windows可执行程序,支持多服务器切换及其对应的上传配置


# 使用场景

>作为开发人员，我们的工作就是为终端用户实现过程自动化；然而，很多开发人员却忽略了将自己的开发过程自动化的机会。

>在开发或测试的过程中，修改代码，打包，上传，重启... 大把的时间花费在这些重复无味的工作上。我们应该把时间花费在更有意义的事情上。我们可以尝试借助一些工具，让这些重复机械的工作交给计算机去完成，这也是我们做软件开发的核心思想。


# 可执行程序的使用(需要JRE)

 **如图** 
1. 先新建连接
2. 新建本地目标源与上传目标目录
3. 点击上传按钮即可

![输入图片说明](https://git.oschina.net/uploads/images/2017/0515/111204_b7c3bcf8_757839.jpeg "在这里输入图片标题")

该程序只是图形界面,为更方便地使用本工具而开发,核心代码都在jar包,通过使用命令行调用jar包并获取其返回的信息。下面是jar包的参数说明


# 参数说明
1. 执行参数

本项目编译出来的是jar包。直接使用maven直接可将相关依赖一起打包，下面是jar包参数说明

参数是一个字符串数组
```
public static void main(String[] args) throws Exception {
            ip = args[0];//服务器IP
            user = args[1];//账号
            pwd = args[2];//密码
            port = Integer.parseInt(args[3]);//端口

            shutdown = args[4];//关闭web服务器的命令
            startup = args[5];//启动web服务器的命令

            //   上传设置,
            //   0若已存在文件,询问是否覆盖
            //   1不询问直接覆盖已存在文件
            //   2先删除目标文件夹/文件,再上传
            uploadType = Integer.parseInt(args[6]);

            //是否通过记录文件MD5,不上传相同文件,这里是是填y/n
            SFTP.isRepeatUpload = args[7].equals("y") ? true : false;
}

```
执行jar例子

```
java -jar  autoDeploy.jar 服务器ip 登录账号 密码 端口 "ps -ef|grep tomcat|grep -v grep|cut -c 9-15|xargs kill -9" "/usr/local/tomcat/bin/startup.sh" 1 y
```
建议关闭web服务器的命令使用kill,如
> ps -ef|grep tomcat|grep -v grep|cut -c 9-15|xargs kill -9
>  
直接终止所有带tomcat关键字的进程,确保tomcat被关闭

 **特别说明：** 不上传重复文件并非直接用本地文件与服务器上的文件进行对比。而是上传一次后将记录该文件的MD5到配置文件中，下次上传进行对比。有必要时可清理该配置文件



2.上传配置文件(conf.ini)
在jar包根目录会有一个conf.ini，记录上传源文件与目标文件夹，格式如下(支持多个节点,若有多个目标,在下面新增一个upload节点即可)
```
[upload]
#本地文件夹/文件
sourcePath=F:/项目/web/WEB-INF/classes/com
#上到服务器的某目录,注意,是目录
destinationPath=/usr/local/tomcat/webapps/项目/WEB-INF/classes

#最终效果是将com包上传到classes目录下
```



# 备注

1.上述的可执行程序由e语言编写,非java。可能会存在误报

2.本工具适用于小型团队开发人员、测试人员使用。

3.不要在生产环境使用

 **本工具初衷是为开发人员方便、节省不必要的部署项目时间。** 

**本质上只是个小工具，和自动化部署还有很大的区别，如数据库、svn/git、版本回滚等，但已满足本人目前所需**