package com.autoDeploy.main;

import com.autoDeploy.ssh.Check;
import com.autoDeploy.ssh.SFTP;
import com.autoDeploy.ssh.Shell;
import com.autoDeploy.util.ConfUtil;
import com.jcraft.jsch.*;
import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.*;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import java.util.Vector;

/**
 * @author HeyS1
 * @date 2017/5/8
 * @description
 */
public class Main {
    public static void main(String[] args) throws Exception {
        String ip = null;
        String user = null;
        String pwd = null;
        Integer port = null;
        Integer uploadType = null;//0询问 1覆盖 2删除再上传


        String shutdown = null;
        String startup = null;


        List<Profile.Section> uploadSection = null;
        try {
            ip = args[0];
            user = args[1];
            pwd = args[2];
            port = Integer.parseInt(args[3]);

            shutdown = args[4];
            startup = args[5];

            uploadType = Integer.parseInt(args[6]);
            //设置是否记录文件MD5
            SFTP.isRepeatUpload = args[7].equals("y") ? true : false;
            uploadSection = ConfUtil.getAllIniValue(ConfUtil.UPLOAD);
        } catch (Exception e) {
            System.out.println("配置参数错误!");
        }

        Session session = SFTP.loginByPassword(ip, user, pwd, port);
        if (session == null) {
            System.exit(0);
        }

        long expendTime = System.currentTimeMillis();
        for (Profile.Section s : uploadSection) {
            String sourcePath = s.get(ConfUtil.SOURCE_PATH);
            String destinationPath = s.get(ConfUtil.DESTINATION_PATH);
            SFTP.upLoadFile(session, sourcePath, destinationPath, uploadType);
        }

        session.disconnect();


        Shell shell = new Shell(ip, port, user, pwd);
        System.out.println("正在重启web服务器...");
        //关闭服务器
        shell.executeCommands(new String[]{shutdown});
        System.out.println(shell.getResponse());
        Thread.sleep(200);
        //启动服务器
        shell.executeCommands(new String[]{startup});
        System.out.println(shell.getResponse());
        System.out.println("重启web服务器成功");
        shell.disconnect();
        expendTime = (System.currentTimeMillis() - expendTime) / 1000;
        System.out.println("=================================部署完毕,耗时" + expendTime + "s=================================");
    }


}
