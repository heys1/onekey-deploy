package com.autoDeploy.util;

import com.autoDeploy.main.Main;
import org.ini4j.Config;
import org.ini4j.Ini;
import org.ini4j.Profile;

import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * @author HeyS1
 * @date 2017/5/8
 * @description
 */
public class ConfUtil {
    public static String CONNECT = "connect";
    public static String SHELL = "shell";
    public static String UPLOAD = "upload";
    public static String SOURCE_PATH = "sourcePath";
    public static String DESTINATION_PATH = "destinationPath";

    public static String confPath;


    static {
        try {
            confPath = getPath() + "/conf.ini";
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws ClassNotFoundException, IOException {
        System.out.println(confPath);
        List<Profile.Section> a= ConfUtil.getAllIniValue(ConfUtil.UPLOAD);
        System.out.println(a);
    }

    public static List<Profile.Section> getAllIniValue(String section) throws IOException {
        Config cfg = new Config();
        // 设置Section允许出现重复
        cfg.setMultiSection(true);

        Ini ini = new Ini();
        ini.setConfig(cfg);
        // 加载配置文件
        ini.load(new File(confPath));
        return ini.getAll(section);
    }

    public static String getIniValue(String section, String key) throws IOException {
        return getIniValue(section, key, confPath);
    }

    public static String getIniValue(String section, String key, String path) {
        try {
            Ini ini = new Ini();
            // 加载配置文件
            ini.load(new File(path));
            Profile.Section s = ini.get(section);
            return s.get(key);
        } catch (Exception e) {
            return null;
        }
    }

    public static void putIniValue(String section, String key, String value) throws IOException {
        File iniFile = new File(confPath);
        Ini ini = new Ini();
        ini.load(iniFile);
        ini.put(section, key, value);
        ini.store(iniFile);
    }

    public static void addIniValue(String section, String key, String value) throws IOException {
        File iniFile = new File(confPath);

        Config cfg = new Config();
        // 设置Section允许出现重复
        cfg.setMultiSection(true);
        Ini ini = new Ini();
        ini.setConfig(cfg);

        ini.load(iniFile);
        ini.add(section, key, value);

        ini.store(iniFile);
    }


    /**
     * 取Main运行目录
     *
     * @return
     * @throws ClassNotFoundException
     */
    public static String getPath() throws ClassNotFoundException {
        URL url = Main.class.getProtectionDomain().getCodeSource().getLocation();
        String filePath = null;
        try {
            filePath = URLDecoder.decode(url.getPath(), "utf-8");// 转化为utf-8编码
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (filePath.endsWith(".jar")) {// 可执行jar包运行的结果里包含".jar"
            // 截取路径中的jar包名
            filePath = filePath.substring(0, filePath.lastIndexOf("/") + 1);
        }

        File file = new File(filePath);

        // /If this abstract pathname is already absolute, then the pathname
        // string is simply returned as if by the getPath method. If this
        // abstract pathname is the empty abstract pathname then the pathname
        // string of the current user directory, which is named by the system
        // property user.dir, is returned.
        filePath = file.getAbsolutePath();//得到windows下的正确路径
        return filePath;
    }


    /**
     * Properties根据key读取value
     *
     * @param key
     * @param path 文件
     * @return
     */
    public static String readValue(String key, String path) throws IOException {
        Properties props = new Properties();
        InputStream in = new BufferedInputStream(new FileInputStream(path));
        props.load(in);
        String value = props.getProperty(key);
        return value;
    }
}
