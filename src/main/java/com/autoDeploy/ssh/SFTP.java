package com.autoDeploy.ssh;

import com.autoDeploy.main.Main;
import com.autoDeploy.util.ConfUtil;
import com.autoDeploy.util.MD5Util;
import com.jcraft.jsch.*;

import java.io.*;
import java.util.Scanner;
import java.util.Vector;

/**
 * @author 大部分code来自互联网
 * @date 2017/5/8
 * @description 连接SFTP
 */
public class SFTP {
    public static boolean isRepeatUpload = true;

    /**
     * 上传文件夹
     *
     * @param session
     * @param sPath   本地源文件夹(也可以是单一文件) D:/文件夹
     * @param dPath   服务器上的文件夹 /usr
     * @param type    0已存在,询问后操作  1已存在,直接覆盖 2已存在,删除再覆盖
     *                <p>
     *                结果：将sPath上传到dPath下
     *                D:/文件夹 -> /usr/文件夹
     */
    public static void upLoadFile(Session session, String sPath, String dPath, int type) {
        Channel channel = null;
        try {
            channel = session.openChannel("sftp");
            channel.connect(10000000);
            ChannelSftp sftp = (ChannelSftp) channel;

            //进入dPath，若dPath不存在会抛异常
            sftp.cd(dPath);
            //源文件夹名
            String mkdName = Check.getFileNameByPath(sPath);
            try {
                //CD进目标文件夹若不报错，该文件夹已存在
                sftp.cd(mkdName);
                //返回上一级,即dPath
                sftp.cd("../");
                if (type == 0) {
                    Scanner scanner = new Scanner(System.in);
                    System.out.println(sftp.pwd() + "/" + mkdName + ":此目录已存在,文件可能会被覆盖!是否继续y/n?");
                    String next = scanner.next();
                    if (!next.toLowerCase().equals("y")) {
                        //终止操作
                        return;
                    }
                } else if (type == 2) {
                    System.out.println("删除再上传：" + sPath + " -> " + dPath + "/" + mkdName);
                    //递归删除该文件夹内所有文件
                    exeRmRec(mkdName, sftp);
                } else {
                    //直接覆盖
                    System.out.println("正在上传并覆盖文件夹：" + sPath + " -> " + dPath + "/" + mkdName);
                }
            } catch (SftpException e) {
                //目标文件夹不存在，直接上传
                System.out.println("正在上传文件夹：" + sPath + " -> " + dPath + "/" + mkdName);
            }
            File file = new File(sPath);
            copyFile(sftp, file, sftp.pwd());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (channel != null) {
                channel.disconnect();
            }
        }
    }

    public static void copyFile(ChannelSftp sftp, File file, String pwd) {
        if (file.isDirectory()) {
            File[] list = file.listFiles();
            try {
                String fileName = file.getName();

                try {
                    sftp.cd(pwd);
                    System.out.println("扫描目录:" + sftp.pwd() + "/" + fileName);
                    sftp.mkdir(fileName);
                } catch (Exception e) {
                    //若文件夹已存在,会抛异常。但无需处理
                }
                pwd = pwd + "/" + fileName;
                try {
                    sftp.cd(fileName);
                } catch (SftpException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //递归
            for (int i = 0; i < list.length; i++) {
                copyFile(sftp, list[i], pwd);
            }
        } else {
            //是否记录上传文件的MD5，下次上传时不上传相同文件
            if (isRepeatUpload) {
                try {
                    String host = sftp.getSession().getHost();
                    if (Check.checkMD5(file, host)) {
                        //配置文件内记录的MD5与当前文件一致，不上传
                        return;
                    } else {
                        Check.putIniValue("md5", file.getName(), MD5Util.getMd5ByFile(file), host);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            System.out.println("  上传文件==>" + file.getName());
            //if (1 == 1)
            //return;
            try {
                sftp.cd(pwd);
            } catch (SftpException e1) {
                e1.printStackTrace();
            }
            InputStream instream = null;
            OutputStream outstream = null;
            try {
                outstream = sftp.put(file.getName());
                instream = new FileInputStream(file);
                byte b[] = new byte[1024];
                int n;
                while ((n = instream.read(b)) != -1) {
                    outstream.write(b, 0, n);
                }
            } catch (SftpException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    outstream.flush();
                    outstream.close();
                    instream.close();
                } catch (Exception ignored) {

                }
            }
        }
    }


    /**
     * 递归删除执行.
     *
     * @param pathString 文件路径
     * @param sftp       sftp连接
     * @throws SftpException
     */
    private static void exeRmRec(final String pathString, final ChannelSftp sftp) throws SftpException {
        Vector<ChannelSftp.LsEntry> vector = sftp.ls(pathString);
        if (vector.size() == 1) { // 文件，直接删除
            sftp.rm(pathString);
        } else if (vector.size() == 2) { // 空文件夹，直接删除
            sftp.rmdir(pathString);
        } else {
            String fileName = "";
            // 删除文件夹下所有文件
            for (ChannelSftp.LsEntry en : vector) {
                fileName = en.getFilename();
                if (".".equals(fileName) || "..".equals(fileName)) {
                    continue;
                } else {
                    exeRmRec(pathString + "/" + fileName, sftp);
                }
            }
            // 删除文件夹
            sftp.rmdir(pathString);
        }
    }

    /**
     * 密码方式登录
     *
     * @param ip
     * @param user
     * @param psw
     * @param port
     */
    public static Session loginByPassword(String ip, String user, String psw, int port) {
        System.out.println("");
        System.out.println("");
        System.out.println("Login...");
        if (isNullOrEmptyString(ip, user, psw)) {
            System.out.println("登录信息不能为空");
            return null;
        }

        Session session = null;

        JSch jsch = new JSch();
        try {
            if (port <= 0) {
                // 连接服务器，采用默认端口
                session = jsch.getSession(user, ip);
            } else {
                // 采用指定的端口连接服务器
                session = jsch.getSession(user, ip, port);
            }

            // 如果服务器连接不上，则抛出异常
            if (session == null) {
                throw new Exception("session is null");
            }

            // 设置登陆主机的密码
            session.setPassword(psw);// 设置密码
            // 设置第一次登陆的时候提示，可选值：(ask | yes | no)
            session.setConfig("StrictHostKeyChecking", "no");
            // 设置登陆超时时间
            session.connect(300000);
            System.out.println("Login Success!");
            return session;
        } catch (Exception e) {
            System.out.println("Login Fail!");
            e.printStackTrace();
            return null;
        }


    }

    /**
     * 密匙方式登录
     *
     * @param ip
     * @param user
     * @param port
     * @param privateKey
     * @param passphrase
     */
    public static Session loginByKey(String ip, String user, int port, String privateKey, String passphrase) {
        System.out.println("privateKey login");
        Session session = null;
        JSch jsch = new JSch();
        try {
            // 设置密钥和密码
            // 支持密钥的方式登陆，只需在jsch.getSession之前设置一下密钥的相关信息就可以了
            if (privateKey != null && !"".equals(privateKey)) {
                if (passphrase != null && "".equals(passphrase)) {
                    // 设置带口令的密钥
                    jsch.addIdentity(privateKey, passphrase);
                } else {
                    // 设置不带口令的密钥
                    jsch.addIdentity(privateKey);
                }
            }
            if (port <= 0) {
                // 连接服务器，采用默认端口
                session = jsch.getSession(user, ip);
            } else {
                // 采用指定的端口连接服务器
                session = jsch.getSession(user, ip, port);
            }
            // 如果服务器连接不上，则抛出异常
            if (session == null) {
                throw new Exception("session is null");
            }
            // 设置第一次登陆的时候提示，可选值：(ask | yes | no)
            session.setConfig("StrictHostKeyChecking", "no");
            // 设置登陆超时时间
            session.connect(300000);
            System.out.println("success");
            return session;
        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;

    }

    /**
     * 判断字符串是否为空文本或null,其中一个参数满足返回True
     *
     * @param args
     * @return
     */
    public static boolean isNullOrEmptyString(String... args) {
        for (String arg : args) {
            if (arg == null || arg.equals(""))
                return true;
        }
        return false;
    }
}
