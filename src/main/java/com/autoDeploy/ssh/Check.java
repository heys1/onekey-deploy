package com.autoDeploy.ssh;

import com.autoDeploy.util.ConfUtil;
import com.autoDeploy.util.MD5Util;
import org.ini4j.Ini;

import java.io.*;
import java.math.BigInteger;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.util.Properties;

/**
 * @author HeyS1
 * @date 2017/5/9
 * @description 用于记录上传项目文件的MD5。再次上传同一目录时，
 * 若文件与配置文件夹内记录的MD5相同，则不重复上传
 */
public class Check {

    public static void main(String[] args) throws IOException {



    }

    /**
     * 记录一个文件的MD5
     * @param section
     * @param key
     * @param value
     * @param host
     * @throws Exception
     */
    public static void putIniValue(String section, String key, String value, String host) throws Exception {
        String path = ConfUtil.getPath() + "/" + host + ".ini";

        File iniFile = new File(path);
        if (!iniFile.exists()) {
            iniFile.createNewFile();
        }

        Ini ini = new Ini();
        ini.load(iniFile);
        ini.put(section, key, value);
        ini.store(iniFile);
    }

    /**
     * 检测文件MD5是否与配置文件内记录的MD5相同
     *
     * @param file
     * @return
     */
    public static boolean checkMD5(File file, String host) throws ClassNotFoundException {


        try {
            String path = ConfUtil.getPath() + "/" + host + ".ini";
            String thisFileMD5 = MD5Util.getMd5ByFile(file);
            String propFileMD5 = ConfUtil.getIniValue("md5", file.getName(), path);
            if (propFileMD5 != null && thisFileMD5.equals(propFileMD5)) {
                return true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


//    /**
//     * 生成一个新的MD5配置文件
//     *
//     * @param path
//     * @throws IOException
//     */
//    public static void generateMD5Prop(String path) throws IOException {
//        //删除旧配置文件
//        File file = new File(md5Path);
//        file.delete();
//        //生成配置文件
//        traverseFolder(path);
//    }
//
//    /**
//     * 遍历某目录文件，并生成一个配置文件记录其MD5
//     *
//     * @param path
//     * @throws IOException
//     */
//    private static void traverseFolder(String path) throws IOException {
//        FileOutputStream oFile = new FileOutputStream(md5Path, true);//true表示追加打开
//        try {
//            File file = new File(path);
//            if (file.exists()) {
//                File[] files = file.listFiles();
//                if (files.length == 0) {
//                    return;
//                } else {
//                    for (File file2 : files) {
//                        if (file2.isDirectory()) {
//                            traverseFolder(file2.getAbsolutePath());
//                        } else {
//                            String md5 = MD5Util.getMd5ByFile(file2);
//                            //记录配置文件
//                            Properties prop = new Properties();
//                            prop.setProperty(file2.getAbsolutePath(), md5);
//                            prop.store(oFile, null);
//                        }
//                    }
//                }
//            } else {
//                System.out.println("文件不存在!");
//            }
//        } finally {
//            oFile.close();
//        }
//    }


    public static String getFileNameByPath(String path) {
        String s1[] = path.split("/");
        String s2[] = path.split("\\u005C");
        String name;
        if (s1.length > s2.length) {
            name = s1[s1.length - 1];
        } else {
            name = s2[s2.length - 1];
        }
        return name;
    }

}
